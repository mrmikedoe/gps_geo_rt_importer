FROM gps-registry:5000/gps-base:latest
RUN mkdir -p /opt/expedia/rt-importer

WORKDIR /opt/expedia/rt-importer

COPY target/*.jar /opt/expedia/rt-importer/rt-importer.jar

EXPOSE 80

CMD ["java" , "-jar", "-Dspring.profiles.active=${PROFILE}","-Dspring.cloud.config.profile=${PROFILE}", "-Dspring.cloud.config.uri=${CONFIG_SERVER}","-Dspring.boot.admin.url=${ADMIN_SERVER}", "/opt/expedia/rt-importer/rt-importer.jar"]
 
