package com.expedia.gps.geo.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mdolmatov on 9/7/15.
 */
@RestController
public class ApiController {

  @Value("${gps.geo.db}")
  private String dbName;

  @RequestMapping(value = "/api/controller", method = RequestMethod.GET)
  public String[] getCategories() {
    return new String[]{dbName};
  }
}
